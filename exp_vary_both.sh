#!/bin/bash

ensemble_size=4
dataset=cifar10


# gr and depth
grs=(12 28 48)
depths=(40 64 88)

# Run for 30 epochs 
dir=vary_both_e_"${ensemble_size}"_"${dataset}"_30
for gr in ${grs[@]}
do
    for depth in ${depths[@]}
    do
    subdir="${dir}"/gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}"
    python run.py --command \""python3 main.py --exp_dir="${subdir}" --run_ensemble=True --depth="${depth}" --growth_rate="${gr}" --ensemble_size="${ensemble_size}" --n_epochs=30 --batch_size=128 --vary_growth_rate=True --vary_depth=True --dataset="${dataset}""\" --job_name gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}" --time 96
    done

done

# Run for 60 epochs 
dir=vary_both_e_"${ensemble_size}"_"${dataset}"_60
for gr in ${grs[@]}
do
    for depth in ${depths[@]}
    do
    subdir="${dir}"/gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}"
    python run.py --command \""python3 main.py --exp_dir="${subdir}" --run_ensemble=True --depth="${depth}" --growth_rate="${gr}" --ensemble_size="${ensemble_size}" --n_epochs=60 --batch_size=128 --vary_growth_rate=True --vary_depth=True --dataset="${dataset}""\" --job_name gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}" --time 96
    done

done

# Run for 90 epochs 
dir=vary_both_e_"${ensemble_size}"_"${dataset}"_90
for gr in ${grs[@]}
do
    for depth in ${depths[@]}
    do
    subdir="${dir}"/gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}"
    python run.py --command \""python3 main.py --exp_dir="${subdir}" --run_ensemble=True --depth="${depth}" --growth_rate="${gr}" --ensemble_size="${ensemble_size}" --n_epochs=90 --batch_size=128 --vary_growth_rate=True --vary_depth=True --dataset="${dataset}""\" --job_name gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}" --time 96
    done

done