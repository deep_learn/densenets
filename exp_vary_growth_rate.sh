#!/bin/bash

# Constant depth
depth=40
ensemble_size=4
dataset=cifar100
    
# Variable gr
for var in "$@"
do
    # run.py is at the rc server
    gr="${var}"
    python run.py --command \""python3 main.py --exp_dir=gr_"${gr}"_d_"${depth}"_e_"${ensemble_size}"_"${dataset}" --run_ensemble=True --depth="${depth}" --growth_rate="${gr}" --ensemble_size="${ensemble_size}" --n_epochs=120 --batch_size=128 --vary_growth_rate=True --dataset="${dataset}""\" --job_name gr"${gr}"_d_"${depth}"_e_"${ensemble_size}" --time 24
done