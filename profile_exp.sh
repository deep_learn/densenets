# python -m cProfile -o ./runs/s.prof main.py --exp_dir=prof_s --run_ensemble=False --depth=100 --growth_rate=12 --ensemble_size=1 --n_epochs=1 --batch_size=64

# python -m cProfile -o ./runs/e4.prof main.py --exp_dir=temp_e4 --run_ensemble=True --depth=100 --growth_rate=12 --ensemble_size=4 --n_epochs=1 --batch_size=64

# python -m cProfile -o ./runs/e8.prof main.py --exp_dir=temp_e8 --run_ensemble=True --depth=100 --growth_rate=12 --ensemble_size=8 --n_epochs=1 --batch_size=64

# python -m cProfile -o ./runs/e12.prof main.py --exp_dir=temp_e12 --run_ensemble=True --depth=100 --growth_rate=12 --ensemble_size=12 --n_epochs=1 --batch_size=64

python -m cProfile -o ./runs/profiling/s_e4.prof main.py --exp_dir=s_e4 --run_ensemble=False --depth=100 --growth_rate=6 --ensemble_size=1 --n_epochs=1 --batch_size=64 --efficient=False
