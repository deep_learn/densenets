import os
import fire

def sync(m='rc', local_dir = ""):
    machines = {}
    
    key = "rc"
    machines[key] = {}
    machines[key]["ip"] = "awasay@login.rc.fas.harvard.edu"
    machines[key]["remote"] = "./densenets/main/runs/vary_both_e_4_cifar10_90/"
    machines[key]["local"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_90/"

    if m not in machines.keys():
        print(m + " not found!")
        print(machines.keys())
        return
    
    if local_dir == "":
        command = "rsync -r " +  machines[m]["ip"]  + ":" + machines[m]["remote"] + " " + machines[m]["local"]
    else:
        command = "rsync -r " +  machines[m]["ip"]  + ":" + machines[m]["remote"] + " " + local_dir
    
    print(command)

    os.system(command)
    # # transfer synced stuff to an all folder
    # os.system("cd results")
    # os.system("mkdir nds-all")
    # os.system("cp -r " + machines[m]["local"] + "/ ./nds-all/")

fire.Fire(sync)

# machines["nds-1"] = {}
    # machines["nds-1"]["ip"] = "wasay-macbook-pro@13.68.220.123" 
    # machines["nds-1"]["remote"] = remote
    # machines["nds-1"]["local"] =  "./results/nds-1"

    # machines["nds-2"] = {}
    # machines["nds-2"]["ip"] = "wasay-macbook-pro@52.191.249.185" 
    # machines["nds-2"]["remote"] = remote
    # machines["nds-2"]["local"] =  "./results/nds-2"

    # machines["nds-3"] = {}

    # machines["nds-3"]["ip"] = "wasay-macbook-pro@20.185.65.215" 
    # machines["nds-3"]["remote"] = remote
    # machines["nds-3"]["local"] =  "./results/nds-3"

    # machines["nds-4"] = {}
    # machines["nds-4"]["ip"] = "wasay-macbook-pro@40.71.112.197" 
    # machines["nds-4"]["remote"] = remote
    # machines["nds-4"]["local"] =  "./results/nds-4"
