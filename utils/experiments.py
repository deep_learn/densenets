# Information about all available experiments
exp = {}

#####################################################################################

key = 'vary_both_e_4_cifar10_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar10_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_svhn_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/svhn_e4/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "svhn"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_svhn_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/svhn_e4/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "svhn"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_cifar10_v_trials_2'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar10_trials_2/"
exp[key]["directory"] = "/n/home04/awasay/densenets/main/runs/vary_both_e_4_cifar10_trials_2/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
# exp[key]["gr_list"] = [12]
# exp[key]["d_list"] = [40]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar10_h_trials_2'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar10_trials_2/"
exp[key]["directory"] = "/n/home04/awasay/densenets/main/runs/vary_both_e_4_cifar10_trials_2/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
# exp[key]["gr_list"] = [12]
# exp[key]["d_list"] = [40]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"


key = 'vary_both_e_6_cifar10_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_6_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 6
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"

key = 'vary_both_e_6_cifar10_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_6_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 6
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_8_cifar10_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_8_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 8
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"

key = 'vary_both_e_8_cifar10_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_8_cifar10/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 8
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_cifar100_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar100/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar100"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar100_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_cifar100/"
exp[key]["gr_list"] = [12, 20, 28, 36, 48]
exp[key]["d_list"] = [40, 64, 88, 112]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar100"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_imagenet_v'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_imagenet_bs_64/"
exp[key]["gr_list"] = [28,36]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "imagenet"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_imagenet_h'
exp[key] = {}
exp[key]["directory"] = "/Users/wasay/code/EvD/densenets/results/rc/vary_both_e_4_imagenet_bs_64/"
exp[key]["gr_list"] = [28,36]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "imagenet"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_imagenet32_v'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_imagenet32/"
exp[key]["gr_list"] = [28,36]
exp[key]["d_list"] = [40,64,88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "imagenet32"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_imagenet32_h'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_imagenet32/"
exp[key]["gr_list"] = [28,36]
exp[key]["d_list"] = [40,64,88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "imagenet32"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

#####################################################################################

key = 'vary_both_e_4_cifar10_30_v'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_30/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar10_30_h'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_30/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_cifar10_60_v'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_60/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar10_60_h'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_60/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

key = 'vary_both_e_4_cifar10_90_h'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_90/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "d"


key = 'vary_both_e_4_cifar10_90_v'
exp[key] = {}
exp[key]["directory"] = "/Users/mba/code/densenets/results/rc/vary_both_e_4_cifar10_90/"
exp[key]["gr_list"] = [12, 28, 48]
exp[key]["d_list"] = [40, 64, 88]
exp[key]["ens_size"] = 4
exp[key]["plot_prefix"] = key
exp[key]["dataset"] = "cifar10"
exp[key]["nameformat"] = "gdedv"
exp[key]["vary"] = "gr"

